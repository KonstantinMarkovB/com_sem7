﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClentSharp
{
    class Program
    {
        static void Main(string[] args)


        {
            try
            {
                Type ShellType = Type.GetTypeFromProgID("WordCheckerSSLab7.component");
                Object ShellObj = Activator.CreateInstance(ShellType);

                string[] words = { "hello", "hallo",  "go", "by" };

                Object obj;
                foreach (string s in words)
                {
                    int res = check(ShellType, ShellObj, s);
                    showMes(s, res);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }

            Console.ReadKey();
        }

        private static int check(Type ShellType, Object ShellObj, string word)
        {
            Object res;
            res = ShellType.InvokeMember("ClearWord", BindingFlags.InvokeMethod, null, ShellObj, new object[] { });
//            Console.WriteLine((int)res);
            foreach(char c in word)
            {
                res = ShellType.InvokeMember("AddToWord", BindingFlags.InvokeMethod, null, ShellObj, new object[] { c });
//                Console.WriteLine((int)res);
            }

            return (int) ShellType.InvokeMember("Check", BindingFlags.InvokeMethod, null, ShellObj, new object[] { });

        }

        private static void showMes(string wort, int ansver)
        {
            if(ansver == 0)
            {
                Console.WriteLine("Correct:   " + wort);
            } else
            {
                Console.WriteLine("Incorrect: " + wort);
            }
        }
    }
}
