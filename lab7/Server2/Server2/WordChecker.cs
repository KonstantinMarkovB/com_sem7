﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Server2
{
    [Guid("59B6897C-2CFF-418A-90D2-D59E8F90D18F")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComSourceInterfaces(typeof(IComEvent))]
    [ProgId("WordCheckerSSLab7.component")]
    public class WordChecker : IWordChecker
    {
        private List<string> words = new List<string>();
        private string word = "";
        public WordChecker() {
            words.Add("hello");
            words.Add("by");
            words.Add("home");
            words.Add("he");
        }

        public int AddToWord(char ch)
        {
            this.word += ch;
            return this.word.Length;
        }

        public int Check()
        {
            if (words.Contains(this.word)) return 0;
            return -1;
        }

        public int ClearWord()
        {
            this.word = "";
            return 0;
        }
    }
}
