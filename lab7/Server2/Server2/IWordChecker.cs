﻿using System;
using System.Runtime.InteropServices;

namespace Server2
{
    [Guid("FC84DEAF-30FB-487A-A5D2-2476516FBA91")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]   
    public interface IWordChecker
    {
        [DispId(1)]
        int Check();

        [DispId(2)]
        int AddToWord(char word);

        [DispId(3)]
        int ClearWord();
    }
}
