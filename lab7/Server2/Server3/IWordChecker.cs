﻿using System;
using System.Runtime.InteropServices;

namespace Server3
{
    [Guid("D432C1F4-EBAB-4FAB-B95D-125F0F9169E1")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]   
    public interface IWordChecker
    {
        [DispId(1)]
        long Check(long word);
    }
}
