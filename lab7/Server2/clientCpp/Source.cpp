#include <windows.h> 
#include <tchar.h> 
#include <iostream> 
#include <initguid.h> 
#include "IWordChecker.h"
#include <system_error>


using namespace std;

int check(IWordChecker* checker, string word) {
	int res;

	checker->ClearWord(&res);
	
	for (char c : word) checker->AddToWord(c, &res);
	
	checker->Check(&res);
	
	return res;
}

int main(int argc, char* argv[])
{
	//SetConsoleCP(CP_UTF8);
	//SetConsoleOutputCP(CP_UTF8);
	if (FAILED(CoInitialize(NULL)))
	{
		cout << "CoInitialize error " << endl;
		return -1;
	}
	char tmp[] = "WordCheckerSSLab7.component";
	char* szProgID = tmp;

	WCHAR szWideProgID[128];
	CLSID clsid;
	long lLen = MultiByteToWideChar(CP_ACP, 0, szProgID, strlen(szProgID), szWideProgID, sizeof(szWideProgID));
	szWideProgID[lLen] = '\0';
	HRESULT hr = ::CLSIDFromProgID(szWideProgID, &clsid);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "  CLSID  ProgID . HR = " << hr << endl;
		return -1;
	}
	IClassFactory* pCF;
	hr = CoGetClassObject(clsid, CLSCTX_INPROC, NULL, IID_IClassFactory, (void**)&pCF);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "    . HR = " << hr << endl;
		return -1;
	}
	IUnknown* pUnk;
	hr = pCF->CreateInstance(NULL, IID_IUnknown, (void**)&pUnk);
	pCF->Release();
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "    . HR = " << hr << endl;
		return -1;
	}
	cout << "  " << endl;
	IWordChecker* pChecker = NULL;
	hr = pUnk->QueryInterface(IID_WordChecker, (LPVOID*)&pChecker);
	pUnk->Release();
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "QueryInterface()  IWordChecker  . HR = " << hr << endl;
		return -1;
	}


	cout << "he    : " << check(pChecker, "he") << endl;
	cout << "hello : " << check(pChecker, "hello") << endl;
	cout << "Hallo : " << check(pChecker, "hallo") << endl;

	pChecker->Release();
	CoUninitialize();
	return 0;
}
