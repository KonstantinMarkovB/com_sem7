
#include <string>
// {FC84DEAF-30FB-487A-A5D2-2476516FBA91}
DEFINE_GUID(IID_WordChecker,
	0xfc84deaf, 0x30fb, 0x487a, 0xa5, 0xd2, 0x24, 0x76, 0x51, 0x6f, 0xba, 0x91);


class IWordChecker : public IUnknown
{
public:
	STDMETHOD(Check(int *))PURE;
	STDMETHOD(AddToWord(char, int*)) PURE;
	STDMETHOD(ClearWord(int*)) PURE;
};
