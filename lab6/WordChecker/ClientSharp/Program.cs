﻿using System;
using System.Reflection;
using System.Text;

namespace ClientSharp
{
    class Program
    {
        //[STAThread]
        static void Main(string[] args)
        {
            WordCheckerLib.IMyWordCheckerSS wc = new WordCheckerLib.MyWordCheckerSS();

            //Type ShellType = Type.GetTypeFromProgID("MyWordCheckerSS.component");
            //Object ShellObj = Activator.CreateInstance(ShellType);
                       
            string[] str = { "hello", "he", "he1", "notepad", "by", "home" };

            int res;
            foreach (string s in str)
            {
                wc.Check(getSBytes(s), out res);

               // ShellType.InvokeMember("Check", BindingFlags.InvokeMethod, null, ShellObj, new object[2] { null, null });

                showResult(s, res);
            }
        }

        public static void showResult(string word, int result)
        {
           
            if (result == 0)
                Console.Write("Correct:   ");
            else
                Console.Write("Incorrect: ");

            Console.WriteLine(word);
        }

        public static sbyte[] getSBytes(string str)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(str); ;
            return Array.ConvertAll(bytes, q => Convert.ToSByte(q));
        }
    }
}
