// dllmain.h : Declaration of module class.

class CWordCheckerModule : public ATL::CAtlDllModuleT< CWordCheckerModule >
{
public :
	DECLARE_LIBID(LIBID_WordCheckerLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_WORDCHECKER, "{9f82083f-12e5-4e46-a53c-480d60574fb7}")
};

extern class CWordCheckerModule _AtlModule;
