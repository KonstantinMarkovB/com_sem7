

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.01.0622 */
/* at Tue Jan 19 06:14:07 2038
 */
/* Compiler settings for WordChecker.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 8.01.0622 
    protocol : all , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */



/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 500
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif /* __RPCNDR_H_VERSION__ */

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __WordChecker_i_h__
#define __WordChecker_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IMyWordCheckerSS_FWD_DEFINED__
#define __IMyWordCheckerSS_FWD_DEFINED__
typedef interface IMyWordCheckerSS IMyWordCheckerSS;

#endif 	/* __IMyWordCheckerSS_FWD_DEFINED__ */


#ifndef __MyWordCheckerSS_FWD_DEFINED__
#define __MyWordCheckerSS_FWD_DEFINED__

#ifdef __cplusplus
typedef class MyWordCheckerSS MyWordCheckerSS;
#else
typedef struct MyWordCheckerSS MyWordCheckerSS;
#endif /* __cplusplus */

#endif 	/* __MyWordCheckerSS_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "shobjidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IMyWordCheckerSS_INTERFACE_DEFINED__
#define __IMyWordCheckerSS_INTERFACE_DEFINED__

/* interface IMyWordCheckerSS */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMyWordCheckerSS;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("53137a1d-bb68-41db-87d6-6a06bc5162e2")
    IMyWordCheckerSS : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Check( 
            /* [in] */ CHAR a[ 11 ],
            /* [out] */ LONG *pResult) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IMyWordCheckerSSVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMyWordCheckerSS * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMyWordCheckerSS * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMyWordCheckerSS * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMyWordCheckerSS * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMyWordCheckerSS * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMyWordCheckerSS * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMyWordCheckerSS * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Check )( 
            IMyWordCheckerSS * This,
            /* [in] */ CHAR a[ 11 ],
            /* [out] */ LONG *pResult);
        
        END_INTERFACE
    } IMyWordCheckerSSVtbl;

    interface IMyWordCheckerSS
    {
        CONST_VTBL struct IMyWordCheckerSSVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMyWordCheckerSS_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IMyWordCheckerSS_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IMyWordCheckerSS_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IMyWordCheckerSS_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IMyWordCheckerSS_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IMyWordCheckerSS_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IMyWordCheckerSS_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IMyWordCheckerSS_Check(This,a,pResult)	\
    ( (This)->lpVtbl -> Check(This,a,pResult) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IMyWordCheckerSS_INTERFACE_DEFINED__ */



#ifndef __WordCheckerLib_LIBRARY_DEFINED__
#define __WordCheckerLib_LIBRARY_DEFINED__

/* library WordCheckerLib */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_WordCheckerLib;

EXTERN_C const CLSID CLSID_MyWordCheckerSS;

#ifdef __cplusplus

class DECLSPEC_UUID("a8931365-86a0-4414-a3fe-060d60b0e30f")
MyWordCheckerSS;
#endif
#endif /* __WordCheckerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


