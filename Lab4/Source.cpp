#include <locale.h>
#include <iostream>
#include <string>
#include <ObjBase.h>
#include <ole2.h>
#include <comdef.h>

using namespace std;

void showError(string mes, HRESULT hr) {
	cout.setf(ios::hex, ios::basefield);
	cout << mes;
	if (hr != NULL) cout << " HR = " << hr;
	cout << endl;
	system("pause");
}
//Microsoft Word . ������� ��������� �������� � ��� ������ ���������� ������������ �� ������ , � ���������� ������ - ����������.

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");

	cout << "������������� ���������� COM" << endl;
	if (FAILED(CoInitialize(NULL))) { showError("������. ������ ���������������� COM.", NULL); return 1; }

	CLSID clsid;
	HRESULT hr = ::CLSIDFromProgID(L"Word.Application", &clsid);
	if (FAILED(hr)) { showError("�� ������� �������� CLSID ��� Word.Application.", hr); return 2; }

	IDispatch* pIDispatch = NULL;
	hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void**)& pIDispatch);
	if (FAILED(hr)) { showError("�� ������� �������� IDispatch ��� ������� Application.", hr); return 3; }


	// #####################################################################################################################

	DISPID dispIDAdd;                   // ����� Add ��������� Presentations
	DISPID dispIDActivePresentation;    // �������� ActivePresentation
	DISPID dispIDSlides;                // ��������� Slides ������� Presentation
	DISPID dispIDAddSlide;              // �������� Slide - ��������� ������ Slides.Add()
	DISPID dispIDSaveAs;                // ����� SaveAs ������� Presentation


	// ====================================================================================================================

	DISPPARAMS noparams = { NULL, NULL, 0, 0 };
	DISPID dispID;
	OLECHAR* name = L"Documents";
	IDispatch* pDocuments = NULL;
	VARIANT result;
	EXCEPINFO excepinfo;
	UINT error;

	hr = pIDispatch->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID ��� ��������� Documents.", hr); return 4; }
	

	hr = pIDispatch->Invoke(dispID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr)) { showError("�� ������� �������� �������� Documents ��� ������� Application.", hr); return 5; }

	pDocuments = result.pdispVal;

	// === OPEN =================================================================================================================
	VARIANT varRetVal;
	VARIANTARG varg;
	varg.vt = VT_BSTR;

	varg.bstrVal = _bstr_t("D:\\Test.docx"); // this is the MS-word document filename, must be changed to a valid filename that exists on disk
	DISPPARAMS dpOpen = { &varg, NULL, 1, 0 };
	DISPID dispOpenID;
	name = OLESTR("Open");

	hr = pDocuments->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispOpenID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID ��� ������ Documents.Open().", hr); return 6; }
	
	hr = pDocuments->Invoke(dispOpenID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dpOpen, &result, &excepinfo, &error);
	if (FAILED(hr)) { showError("�� ������� ������� ����� Documents.Open().", hr); return 7; }

	IDispatch* pDocument = result.pdispVal;

	//====================================================================================================================

	// ��� ���������� ����������� �������� ��������� Slides
	name = OLESTR("Paragraphs");
	DISPID dispParagraphsID;
	hr = pDocument->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispParagraphsID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID for Paragraphs.", hr); return 10; }


	hr = pDocument->Invoke(dispParagraphsID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr)) { showError("�� ������� �������� �������� Paragraphs.", hr); return 11; }

	IDispatch* pDispatchParagraphs = result.pdispVal; 

	//====================================================================================================================

	name = OLESTR("Count");
	DISPID dispCountID;
	hr = pDispatchParagraphs->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispCountID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID for Counts.", hr); return 10; }

	hr = pDispatchParagraphs->Invoke(dispCountID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr)) { showError("�� ������� �������� �������� Counts.", hr); return 11; }

	cout << "------------------------" <<result.lVal << endl;

	name = OLESTR("Item");
	DISPID dispItemID;
	hr = pDispatchParagraphs->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispItemID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID for Item.", hr);  }

	IDispatch* pDispatchParagraph;
	cout << result.lVal << " - count" << endl;
	long count = result.lVal;

	for (long i = 1; i <= count; i++) {
		cout << ">" << i << endl;
		VARIANTARG varg;
		varg.vt = VT_I4;
		varg.lVal = i; 
		DISPPARAMS dpItem = { &varg, NULL, 1, 0 };

		hr = pDispatchParagraphs->Invoke(dispItemID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dpItem, &result, &excepinfo, &error);
		if (FAILED(hr)) { showError("�� ������� �������� Item.", hr);  break; }
		pDispatchParagraph = result.pdispVal;

		// ==========================================================================
		
		DISPID dispRangeID;
		name = OLESTR("Range");

		hr = pDispatchParagraph->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispRangeID);
		if (FAILED(hr)) { showError("�� ������� �������� DISPID for Range.", hr);  break;}

		hr = pDispatchParagraph->Invoke(dispRangeID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
		if (FAILED(hr)) { showError("�� ������� �������� �������� Range.", hr);  break;}

		IDispatch* pDispatchRange = result.pdispVal;

		// ==================================================================================================================

		varg.vt = VT_BOOL;
		varg.boolVal = true; // this is the MS-word document filename, must be changed to a valid filename that exists on disk
		DISPPARAMS dpBlob = { &varg, NULL, 1, 0 };

		name = OLESTR("Bold");
		DISPID dispBlobID;
		hr = pDispatchRange->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispBlobID);
		if (FAILED(hr)) { showError("�� ������� �������� DISPID for Bold.", hr);  break; }

		hr = pDispatchRange->Invoke(dispBlobID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dpBlob, &result, &excepinfo, &error);
		if (FAILED(hr)) { showError("�� ������� set Bold.", hr); break; }

	

		name = OLESTR("Alignment");
		DISPID dispAlignmentID;
		hr = pDispatchParagraph->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispAlignmentID);
		if (FAILED(hr)) { showError("�� ������� �������� DISPID for Alignment.", hr);  break; }

		varg.vt = VT_INT;
		varg.intVal = 3;
		DISPPARAMS dpAligment = { &varg, NULL, 1, 0 };

		hr = pDispatchParagraph->Invoke(dispAlignmentID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dpAligment, &result, &excepinfo, &error);
		if (FAILED(hr)) { showError("�� ������� set Alignment.", hr); break; }
	}



	// ##################################################################################################################



	name = OLESTR("Save");
	DISPID dispSaveID;
	hr = pDocuments->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispSaveID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID for Save.", hr);  }

	hr = pDocuments->Invoke(dispSaveID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr)) { showError("�� ������� invoke  Save.", hr); return 11; }
	
	// ====================================================================================================================
		//pDocuments
	name = OLESTR("Close");
	DISPID dispCloseID;
	hr = pDocuments->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispCloseID);
	if (FAILED(hr)) { showError("�� ������� �������� DISPID for Close.", hr);  }

	hr = pDocuments->Invoke(dispCloseID, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr)) { showError("�� ������� invoke  Close.", hr); return 11; }

	CoUninitialize();
	cout << "���������� COM ���������" << endl;
	return 0;
}