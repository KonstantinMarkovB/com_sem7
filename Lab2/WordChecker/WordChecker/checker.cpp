#include <windows.h>
#include "checker.h"
#include <vector>
#include <fstream>

using namespace std;

string fileName = "\"D:\\VSU\\Semestr 7(31 gr POKS)\COM\\Lab2\\words.txt\"";
std::vector<std::string> dictionary = {};

CRITICAL_SECTION m_criticalSection;

WordChecker::WordChecker() {
	InitializeCriticalSection(&m_criticalSection);
	m_lRef = 0;
	InterlockedIncrement(&g_lObjs);
}


WordChecker::~WordChecker() {
	InterlockedDecrement(&g_lObjs);
	DeleteCriticalSection(&m_criticalSection);
}

STDMETHODIMP WordChecker::QueryInterface(REFIID riid, void** ppv) {
	*ppv = 0;
	if (riid == IID_IUnknown || riid == IID_IWordChecker)
		* ppv = this;

	if (*ppv) {
		AddRef();
		return(S_OK);
	}
	return (E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) WordChecker::AddRef() {
	return InterlockedIncrement(&m_lRef);
}

STDMETHODIMP_(ULONG) WordChecker::Release() {
	if (InterlockedDecrement(&m_lRef) == 0) {
		delete this;
		return 0;
	}

	return m_lRef;
}

// [0] - ok
// [other] - incorrect

void loadDictionary() {
	/*
		ifstream infile("\"D:\\VSU\\Semestr 7(31 gr POKS)\COM\\Lab2\\words.txt\"", std::ifstream::in);

		string line;

		while (std::getline(infile, line)){
			dictionary.push_back(line);
		}
		
	infile.close();
	*/
	dictionary = {
		"hello", "he", "by", "home", "table", "door"
	};
}

string toLower(string str) {
	string res = "";
	for (char& c : str) {
		res += tolower(c);
	}
	return res;
}

bool isLoaded = false;
STDMETHODIMP WordChecker::checkWord(std::string word, long* pKode, std::string* res) {
	EnterCriticalSection(&m_criticalSection);
	if (!isLoaded) {
		loadDictionary();
		isLoaded = true;
		LeaveCriticalSection(&m_criticalSection);
	}

	*pKode = 1;
	for (std::string s : dictionary) {
		if (s.compare(toLower(word)) == 0) {
			*pKode = 0;
			return S_OK;
		}
	}
	return S_OK;
}




WordCheckerClassFactory::WordCheckerClassFactory() {
	m_lRef = 0;
}

WordCheckerClassFactory::~WordCheckerClassFactory() {}

STDMETHODIMP WordCheckerClassFactory::QueryInterface(REFIID riid, void** ppv) {
	*ppv = 0;
	if (riid == IID_IUnknown || riid == IID_IClassFactory)
		* ppv = this;
	if (*ppv) {
		AddRef();
		return S_OK;
	}
	return (E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) WordCheckerClassFactory::AddRef() {
	return InterlockedIncrement(&m_lRef);
}

STDMETHODIMP_(ULONG) WordCheckerClassFactory::Release()
{
	if (InterlockedDecrement(&m_lRef) == 0)
	{
		delete this;
		return 0;
	}
	return
		m_lRef;
}

STDMETHODIMP WordCheckerClassFactory::CreateInstance(LPUNKNOWN pUnkOuter, REFIID riid, void** ppvObj) {
	WordChecker* pMath;
	HRESULT hr;
	*ppvObj = 0;
	pMath = new WordChecker;
	if (pMath == 0)
		return(E_OUTOFMEMORY);
	hr = pMath->QueryInterface(riid, ppvObj);
	if (FAILED(hr))
		delete pMath;
	return hr;
}

STDMETHODIMP WordCheckerClassFactory::LockServer(BOOL fLock) {
	if (fLock)
		InterlockedIncrement(&g_lLocks);
	else
		InterlockedDecrement(&g_lLocks);
	return S_OK;
}