#include "ichecker.h"

extern long g_lObjs;
extern long g_lLocks;

class WordChecker : public IWordChecker
{
protected:
	long m_lRef;

public:
	WordChecker();
	~WordChecker();

public:
	STDMETHOD(QueryInterface(REFIID, void**));
	STDMETHOD_(ULONG, AddRef());
	STDMETHOD_(ULONG, Release());

	// ��������� IWordChecker
	STDMETHOD(checkWord(std::string, long*, std::string*));

};

class WordCheckerClassFactory : public IClassFactory
{
protected:
	long m_lRef;
public:
	WordCheckerClassFactory();
	~WordCheckerClassFactory();

	// IUncnown
	STDMETHOD(QueryInterface(REFIID, void**));
	STDMETHOD_(ULONG, AddRef());
	STDMETHOD_(ULONG, Release());

	// IClassFactory
	STDMETHOD(CreateInstance(LPUNKNOWN, REFIID, void**));
	STDMETHOD(LockServer(BOOL));
};