#include <windows.h> 
#include <atlbase.h>
#include <tchar.h> 
#include <iostream>
#include <initguid.h> 
#include "ichecker.h" 

using namespace std;

int main(int argc, char* argv[])
{
	SetConsoleOutputCP(1251);
	cout << " ������������� ���������� COM" << endl;
	if (FAILED(CoInitialize(NULL)))
	{
		std::cout << "������.������ ���������������� COM" << std::endl;
		return -1;
	}

	char tmp[] = "Wordchecker.Component.1";
	cout << tmp << endl;
	char* szProgID = tmp;
	WCHAR szWideProgID[128];
	CLSID clsid;
	long lLen = MultiByteToWideChar(CP_ACP, 0, szProgID, strlen(szProgID), szWideProgID, sizeof(szWideProgID));

	szWideProgID[lLen] = '\0';
	HRESULT hr = ::CLSIDFromProgID(szWideProgID, &clsid);
	if (FAILED(hr)) {
		std::cout.setf(ios::hex, ios::basefield);
		std::cout << "������ �������� CLSID ��� ProgID. HR = " << hr << std::endl;
		return -1;
	}
	IClassFactory* pCF;
	hr = CoGetClassObject(clsid, CLSCTX_INPROC, NULL, IID_IClassFactory, (void**)& pCF);

	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		std::cout << "�� ������� ������� ������ �������. HR = " << hr << std::endl;
		return -1;
	}

	IUnknown* pUnk;
	hr = pCF->CreateInstance(NULL, IID_IUnknown, (void**)& pUnk);
	pCF->Release();
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		std::cout << "�� ������� ������� ��������� �������. HR= " << hr << std::endl;
		return -1;
	}

	std::cout << "��������� ������� ������" << std::endl;
	IWordChecker* pChecker = NULL;
	hr = pUnk->QueryInterface(IID_IWordChecker, (LPVOID*)& pChecker);
	pUnk->Release();

	if (FAILED(hr)) {
		std::cout << "QueryInterface() ��� IID_IWordChecker �� ��������" << std::endl;
		return -1;
	}

	long result;
	std::string resultStr;
	std::string word = "hello";

	cout << "Enter your word: "; cin >> word;

	pChecker->checkWord(word, &result, &resultStr);
	if (result == 0) {
		std::cout << word << " - correkt!" << std::endl;
	}
	else
	{
		std::cout << word << " - incorrekt!" << std::endl;
	}


	std::cout << "����������� ���������" << std::endl;
	pChecker->Release();
	std::cout << "����������� ���������� COM" << std::endl;
	CoUninitialize();

	//system("pause");
	return 0;
}