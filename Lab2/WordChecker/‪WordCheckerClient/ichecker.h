/* MIDL: this ALWAYS GENERATED file contains the definitions for the interfaces */
#include <string>
#include <atlhost.h>

// {CB0275A8-B460-46A4-A9DE-00F7CB4720AA}
DEFINE_GUID(CLSID_WordChecker,
	0xcb0275a8, 0xb460, 0x46a4, 0xa9, 0xde, 0x0, 0xf7, 0xcb, 0x47, 0x20, 0xaa);

// {1A8CE466-21E2-4556-A75A-D2B66454EE8A}       
DEFINE_GUID(IID_IWordChecker,
	0x1a8ce466, 0x21e2, 0x4556, 0xa7, 0x5a, 0xd2, 0xb6, 0x64, 0x54, 0xee, 0x8a);

class IWordChecker : public IUnknown {
public:
	STDMETHOD(checkWord(std::string, long*, std::string*)) PURE;
};