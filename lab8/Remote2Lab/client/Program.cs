﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

namespace client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                HttpChannel chan = new HttpChannel(0);
                ChannelServices.RegisterChannel(chan);
                
                MarshalByRefObject objCalc = (MarshalByRefObject)RemotingServices.Connect(typeof(server.ICalc), "http://localhost:4200/cl");
                MarshalByRefObject objWordCh = (MarshalByRefObject)RemotingServices.Connect(typeof(server.IWordChecker), "http://localhost:4200/wc");
                
                server.ICalc cl = objCalc as server.ICalc;
                server.IWordChecker wc = objWordCh as server.IWordChecker;

                double a = 2, b = 2;
                Console.WriteLine("{0} + {1} = {2}", a, b, cl.Add(a, b));
                Console.WriteLine("{0} - {1} = {2}", a, b, cl.Subtract(a, b));
                Console.WriteLine("{0} / {1} = {2}", a, b, cl.Divide(a, b));
                Console.WriteLine("{0} * {1} = {2}", a, b, cl.Multiply(a, b));

                Console.WriteLine("hello : {0}", wc.Check("hello"));
                Console.WriteLine("hollo : {0}", wc.Check("hollo"));
                Console.WriteLine("home : {0}", wc.Check("home"));
            
            }
            catch (Exception e)
            {
                Console.WriteLine("Client Error:{0}", e.ToString());
            }
            Console.ReadKey();

        }
    }
}
