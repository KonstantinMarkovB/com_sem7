﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    class WordChecker : MarshalByRefObject, IWordChecker
    {
        private List<string> words = new List<string>();

        public WordChecker()
        {
            words.Add("hello");
            words.Add("by");
            words.Add("home");
            words.Add("he");
            Console.WriteLine("Create WordChecker");
        }

        public int Check(string word)
        {
            bool contains = words.Contains(word);
            Console.WriteLine("Check: {0}, contains: {1}", word, contains);
            return contains ? 0 : -1;
        }
    }
}
