﻿using System;

namespace server
{
    public class Calc : MarshalByRefObject, ICalc
    {
        public Calc()
        {
            Console.WriteLine("Create Calc class");
        }

        public double Add(double x, double y)
        {
            Console.WriteLine("Add {0} + {1}", x , y);
            return x + y; 
        }

        public double Divide(double x, double y)
        {
            Console.WriteLine("Div {0} / {1}", x, y);
            return x / y;
        }

        public double Multiply(double x, double y)
        {
            Console.WriteLine("Mult {0} * {1}", x, y);
            return x * y;
        }

        public double Subtract(double x, double y)
        {
            Console.WriteLine("Sub {0} - {1}", x, y);
            return x - y;
        }
    }
}
