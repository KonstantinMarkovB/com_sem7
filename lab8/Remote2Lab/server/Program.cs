﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    class Program
    {
        [Obsolete]
        static void Main(string[] args)
        {       
            try 
            {              
                HttpChannel chan = new HttpChannel(4200);
                ChannelServices.RegisterChannel(chan);
				
                Type typeCalc = Type.GetType("server.Calc");
                Type typeWordCh = Type.GetType("server.WordChecker");

                RemotingConfiguration.RegisterWellKnownServiceType(
                    typeCalc, "cl",
                    WellKnownObjectMode.Singleton);
                
                RemotingConfiguration.RegisterWellKnownServiceType(
                    typeWordCh, "wc",
                    WellKnownObjectMode.SingleCall);

            } catch (Exception e)
            {   
                Console.WriteLine("server Error:{0}", e.ToString());
            }

            Console.ReadKey();
        }
    }
}
