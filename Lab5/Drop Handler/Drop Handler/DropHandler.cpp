﻿#include <windows.h>
#include <iostream>
#include <objbase.h>
#include <objbase.h>
#include "DropHandler.h"

void write_text_to_log_file(const string& text)
{
	std::ofstream log_file("D:\\output.txt", ios_base::out | ios_base::app);
	log_file << text;
	log_file << endl;
	log_file.close();
}
void write_text_to_log_file(const int text)
{
	std::ofstream log_file("D:\\output.txt", ios_base::out | ios_base::app);
	log_file << text;
	log_file << endl;
	log_file.close();
}

MyDropHandler::MyDropHandler() {
	m_lRef = 0;
	InterlockedIncrement(&g_lObjs);
}

MyDropHandler::~MyDropHandler() {
	InterlockedDecrement(&g_lObjs);
}

STDMETHODIMP MyDropHandler::QueryInterface(REFIID riid, void** ppv) {
	*ppv = 0;

	/*
	if (riid == IID_IUnknown || 
		riid == IID_IShellExtInit ||
		riid == IID_IPersistFile ||
		riid == IID_IDropTarget
		) *ppv = this;
	*/
	if (IsEqualIID(riid, IID_IUnknown))
		* ppv = this;
	else if (IsEqualIID(riid, IID_IClassFactory))
		* ppv = (IClassFactory*)this;
	else if (IsEqualIID(riid, IID_IShellExtInit))
		* ppv = (IShellExtInit*)this;
	else if (IsEqualIID(riid, IID_IPersistFile))
		* ppv = (IPersistFile*)this;
	else if (IsEqualIID(riid, IID_IDropTarget))
		* ppv = (IDropTarget*)this;

	if (*ppv) {
		AddRef();
		return(S_OK);
	}
	return (E_NOINTERFACE);
}

STDMETHODIMP_(ULONG) MyDropHandler::AddRef() {
	return InterlockedIncrement(&m_lRef);
}

STDMETHODIMP_(ULONG) MyDropHandler::Release() {
	if (InterlockedDecrement(&m_lRef) == 0) {
		delete this;
		return 0;
	}

	return m_lRef;
}

//#################################################################
//#################################################################
//#################################################################
//#################################################################

STDMETHODIMP MyDropHandler::DragEnter(IDataObject* pDataObj, DWORD grfKeyState,POINTL pt, DWORD* pdwEffect){ return S_OK; }

void removeFile(TCHAR fileName[]) {
	write_text_to_log_file("Remove: ");
	write_text_to_log_file(fileName);

	ifstream ifile(fileName, std::ifstream::binary);

	const auto begin = ifile.tellg();
	ifile.seekg(0, ios::end);
	const auto end = ifile.tellg();

	const auto fsize = (end - begin);


	ifile.close();

	write_text_to_log_file("Size: ");
	write_text_to_log_file(fsize);

	ofstream ofile(fileName, ios::binary | std::ofstream::trunc);
	for (int i = 0; i < fsize; i++) ofile << 0;
	ofile.close();

	ofstream ofile2(fileName, std::ofstream::trunc);
	ofile2.close();

	write_text_to_log_file("ok");
	MessageBoxW(NULL, L"Ok", L"MyDropHandler", MB_OK);

	remove(fileName);
}

STDMETHODIMP MyDropHandler::Drop(
	IDataObject* pDataObj, DWORD grfKeyState,
	POINTL pt, DWORD* pdwEffect)
{

	FORMATETC fmte = { CF_HDROP, NULL, DVASPECT_CONTENT,
					   -1, TYMED_HGLOBAL };
	STGMEDIUM stgm;
	if (SUCCEEDED(pDataObj->GetData(&fmte, &stgm))) {
		HDROP hdrop = reinterpret_cast<HDROP>(stgm.hGlobal);
		UINT cFiles = DragQueryFile(hdrop, 0xFFFFFFFF, NULL, 0);
		for (UINT i = 0; i < cFiles; i++) {
			TCHAR szFile[MAX_PATH];
			UINT cch = DragQueryFile(hdrop, i, szFile, MAX_PATH);
			if (cch > 0 && cch < MAX_PATH) {
				removeFile(szFile);
			}
		}
		ReleaseStgMedium(&stgm);
	}

	*pdwEffect &= DROPEFFECT_COPY;
	return S_OK;
}





//#################################################################
//#################################################################
//#################################################################
//#################################################################

DropHandlerClassFactory::DropHandlerClassFactory() {
	m_lRef = 0;
}

DropHandlerClassFactory::~DropHandlerClassFactory(){}

STDMETHODIMP DropHandlerClassFactory::QueryInterface(REFIID riid, void** ppv) {
	//write_text_to_log_file("DropHandlerClassFactory::DropHandlerClassFactory()");
	*ppv = 0;
	if (riid == IID_IUnknown || riid == IID_IClassFactory) {
		*ppv = this;
		AddRef();
		return S_OK;
	}
	else {
		return (E_NOINTERFACE);
	}
	
}

STDMETHODIMP_(ULONG) DropHandlerClassFactory::AddRef() {
	return InterlockedIncrement(&m_lRef);
}

STDMETHODIMP_(ULONG) DropHandlerClassFactory::Release()
{
	if (InterlockedDecrement(&m_lRef) == 0)
	{
		delete this;
		return 0;
	}
	return
		m_lRef;
}

STDMETHODIMP DropHandlerClassFactory::CreateInstance(LPUNKNOWN pUnkOuter, REFIID riid, void** ppvObj) {
	MyDropHandler* pDgDp;
	HRESULT hr;
	*ppvObj = 0;
	pDgDp = new MyDropHandler;

	if (pDgDp == 0) return(E_OUTOFMEMORY);

	hr = pDgDp->QueryInterface(riid, ppvObj);

	if (FAILED(hr)) delete pDgDp;
	return hr;
}

STDMETHODIMP DropHandlerClassFactory::LockServer(BOOL fLock) {
	if (fLock)
		InterlockedIncrement(&g_lLocks);
	else
		InterlockedDecrement(&g_lLocks);
	return S_OK;
}