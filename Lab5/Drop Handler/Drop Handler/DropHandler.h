#include <comdef.h>
#include <shlobj.h>
#include <strsafe.h>
#include <fstream>
#include <locale> 
#include <codecvt>


// {55D05E8A-CA77-4226-891F-FBDD81DC5AAB}
DEFINE_GUID(CLSID_MyDropHandler, 0x55d05e8a, 0xca77, 0x4226, 0x89, 0x1f, 0xfb, 0xdd, 0x81, 0xdc, 0x5a, 0xab);

extern long g_lObjs;
extern long g_lLocks;

using namespace std;

class MyDropHandler : 
	public IPersistFile,
	public IShellExtInit,
	public IDropTarget
{
private:
	LPCOLESTR pszFileName_;
protected:
	long m_lRef;

public:
	MyDropHandler();
	~MyDropHandler();

public:
	STDMETHOD (QueryInterface(REFIID, void**));
	STDMETHOD_(ULONG, AddRef());
	STDMETHOD_(ULONG, Release());

	// IPersistFile ######################################################################
	STDMETHOD(GetClassID(CLSID* pCLSID)) { 
		MessageBoxW(NULL, L"Get CLSID", L"", MB_OK);
		*pCLSID = CLSID_MyDropHandler;
		return S_OK;
	}

	STDMETHODIMP IsDirty() {  return E_NOTIMPL; }

	TCHAR m_szFileName[MAX_PATH];    // The file name
	DWORD m_dwMode;                  // The file access mode
	STDMETHODIMP Load(PCWSTR pszFile, DWORD dwMode)
	{ 
		int len = WideCharToMultiByte(CP_ACP, 0, pszFile, wcslen(pszFile), NULL, 0, NULL, NULL);
		char* buffer = new char[len + 1];
		WideCharToMultiByte(CP_ACP, 0, pszFile, wcslen(pszFile), buffer, len, NULL, NULL);
		buffer[len] = '\0';

		m_dwMode = dwMode;
		StringCchCopy(m_szFileName, ARRAYSIZE(m_szFileName), buffer);

		return S_OK;
	}
	STDMETHODIMP Save(LPCOLESTR f, BOOL) { return E_NOTIMPL; }
	STDMETHODIMP SaveCompleted(LPCOLESTR) { return E_NOTIMPL; }
	STDMETHODIMP GetCurFile(LPOLESTR* f) { return E_NOTIMPL; }
	
	//IShellExtInit ######################################################################
	STDMETHODIMP Initialize(LPCITEMIDLIST pidlFolder, LPDATAOBJECT pDataObj, HKEY hProgID){ return S_OK; }
	

	// IDropTarget ######################################################################
	STDMETHODIMP DragEnter(IDataObject* pDataObj, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect);
	STDMETHODIMP DragOver(DWORD grfKeyState, POINTL pt, DWORD* pdwEffect){ return E_NOTIMPL; }
	STDMETHODIMP DragLeave(){ return E_NOTIMPL; }
	STDMETHODIMP Drop(IDataObject* pDataObj, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect);
		
};

class DropHandlerClassFactory : public IClassFactory
{
protected:
	long m_lRef;
public:
	DropHandlerClassFactory();
	~DropHandlerClassFactory();

	// IUncnown
	STDMETHOD(QueryInterface(REFIID, void**));
	STDMETHOD_(ULONG, AddRef());
	STDMETHOD_(ULONG, Release());

	// IClassFactory
	STDMETHOD(CreateInstance(LPUNKNOWN, REFIID, void**));
	STDMETHOD(LockServer(BOOL));
};