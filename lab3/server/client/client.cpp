#include <windows.h> 
#include <atlbase.h>
#include <tchar.h> 
#include <iostream>
#include <initguid.h> 
#include "iMyMath.h" 

using namespace std;

int main(int argc, char* argv[])
{
	SetConsoleOutputCP(1251);
	cout << " ������������� ���������� COM" << endl;
	if (FAILED(CoInitialize(NULL)))
	{
		std::cout << "������.������ ���������������� COM" << std::endl;
		return -1;
	}

	char tmp[] = "MyMath.Component";
	char* szProgID = tmp;
	WCHAR szWideProgID[128];
	CLSID clsid;
	long lLen = MultiByteToWideChar(CP_ACP, 0, szProgID, strlen(szProgID), szWideProgID, sizeof(szWideProgID));

	szWideProgID[lLen] = '\0';
	HRESULT hr = ::CLSIDFromProgID(szWideProgID, &clsid);
	if (FAILED(hr)) {
		std::cout.setf(ios::hex, ios::basefield);
		std::cout << "������ �������� CLSID ��� ProgID. HR = " << hr << std::endl;
		return -1;
	}
	IClassFactory* pCF;
	hr = CoGetClassObject(clsid, CLSCTX_INPROC, NULL, IID_IClassFactory, (void**)& pCF);

	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		std::cout << "�� ������� ������� ������ �������. HR = " << hr << std::endl;
		return -1;
	}

	IUnknown* pUnk;
	hr = pCF->CreateInstance(NULL, IID_IUnknown, (void**)& pUnk);
	pCF->Release();
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		std::cout << "�� ������� ������� ��������� �������. HR= " << hr << std::endl;
		return -1;
	}

	std::cout << "��������� ������� ������" << std::endl;
	IMyMath* pMath = NULL;
	hr = pUnk->QueryInterface(IID_IMyMath, (LPVOID*)& pMath);
	pUnk->Release();

	if (FAILED(hr)) {
		std::cout << "QueryInterface() ��� IMyMath �� ��������" << std::endl;
		return -1;
	}

	long result;
	pMath->Multiply(100, 8, &result);
	std::cout << "100 * 8 = " << result << std::endl;

	pMath->Subtract(1000, 333, &result);
	std::cout << "1000 - 333 = " << result << std::endl;

	std::cout << "����������� ���������" << std::endl;
	pMath->Release();
	std::cout << "����������� ���������� COM" << std::endl;
	CoUninitialize();
	return 0;
}