/* MIDL: this ALWAYS GENERATED file contains the definitions for the interfaces */

#include <atlhost.h>

// {72F0192A-9FFD-49AF-B30C-7A98BDB6988F}
DEFINE_GUID(CLSID_MyMath,
	0x4892ecd1, 0x221e, 0x4cf6, 0xa4, 0x18, 0x4b, 0x21, 0x06, 0x7e, 0xe0, 0x84);

DEFINE_GUID(IID_IMyMath,
	0x8e59d380, 0xf676, 0x4bbc, 0xbf, 0x19, 0xdc, 0x29, 0x70, 0xb3, 0x38, 0x72);

class IMyMath : public IUnknown
{
public:
	STDMETHOD(Add(long, long, long*))      PURE;
	STDMETHOD(Subtract(long, long, long*)) PURE;
	STDMETHOD(Multiply(long, long, long*)) PURE;
	STDMETHOD(Divide(long, long, long*))   PURE;

};