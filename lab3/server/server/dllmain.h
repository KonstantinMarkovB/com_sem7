// dllmain.h : Declaration of module class.

class CserverModule : public ATL::CAtlDllModuleT< CserverModule >
{
public :
	DECLARE_LIBID(LIBID_serverLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_SERVER, "{a36cfc26-c572-442c-92cd-ada0d73d0d15}")
};

extern class CserverModule _AtlModule;
