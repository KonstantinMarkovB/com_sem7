// MyMath.h : Declaration of the CMyMath

#pragma once
#include "resource.h"       // main symbols

#include "server_i.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CMyMath

class ATL_NO_VTABLE CMyMath :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CMyMath, &CLSID_MyMath>,
	public IMyMath
{
public:
	CMyMath()чат {
	}

DECLARE_REGISTRY_RESOURCEID(106)

DECLARE_NOT_AGGREGATABLE(CMyMath)

BEGIN_COM_MAP(CMyMath)
	COM_INTERFACE_ENTRY(IMyMath)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:



	STDMETHOD(Add)(LONG lOp1, LONG lOp2, LONG* pResult);
	STDMETHOD(Subtract)(LONG lOp1, LONG lOp2, LONG* pResult);
	STDMETHOD(Multiply)(LONG lOp1, LONG lOp2, LONG* pResult);
	STDMETHOD(Divide)(LONG lOp1, LONG lOp2, LONG* pResult);
};

OBJECT_ENTRY_AUTO(__uuidof(MyMath), CMyMath)
