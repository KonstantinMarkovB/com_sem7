// MyMath.cpp : Implementation of CMyMath

#include "pch.h"
#include "MyMath.h"

STDMETHODIMP CMyMath::Add(LONG lOp1, LONG lOp2, LONG* pResult)
{
	*pResult = lOp1 + lOp2;
	return S_OK;
}


STDMETHODIMP CMyMath::Subtract(LONG lOp1, LONG lOp2, LONG* pResult)
{
	*pResult = lOp1 - lOp2;
	return S_OK;
}


STDMETHODIMP CMyMath::Multiply(LONG lOp1, LONG lOp2, LONG* pResult)
{
	*pResult = lOp1 * lOp2;
	return S_OK;
}


STDMETHODIMP CMyMath::Divide(LONG lOp1, LONG lOp2, LONG* pResult)
{
	*pResult = lOp1 / lOp2;
	return S_OK;
}
