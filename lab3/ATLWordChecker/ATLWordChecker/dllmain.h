// dllmain.h : Declaration of module class.

class CATLWordCheckerModule : public ATL::CAtlDllModuleT< CATLWordCheckerModule >
{
public :
	DECLARE_LIBID(LIBID_ATLWordCheckerLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ATLWORDCHECKER, "{b81b7a33-ac7b-45b9-89ef-6a2e81348b96}")
};

extern class CATLWordCheckerModule _AtlModule;
