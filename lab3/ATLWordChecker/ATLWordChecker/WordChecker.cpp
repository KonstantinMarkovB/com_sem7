// WordChecker.cpp : Implementation of CWordChecker

#include "pch.h"
#include "WordChecker.h"
#include <string>
#include <vector>

std::vector<std::string> dictionary = {};

void loadDictionary() {
	dictionary = {
		"hello", "he", "by", "home", "table", "door"
	};
}

std::string toLower(std::string str) {
	std::string res = "";
	for (char& c : str) {
		res += tolower(c);
	}
	return res;
}

bool isLoaded = false;

STDMETHODIMP CWordChecker::CheckWord(CHAR* pWordForCheck, LONG* pResult)
{
	if (!isLoaded) {
		loadDictionary();
		isLoaded = true;
	}

	*pResult = 1;
	std::string word = toLower(std::string(pWordForCheck));

	for (std::string s : dictionary) {
		if (s.compare(word) == 0) {
			*pResult = 0;
			return S_OK;
		}
	}

	return S_OK;
}
