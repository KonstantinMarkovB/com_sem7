

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.01.0622 */
/* at Tue Jan 19 06:14:07 2038
 */
/* Compiler settings for ATLWordChecker.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 8.01.0622 
    protocol : all , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */



/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 500
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif /* __RPCNDR_H_VERSION__ */

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ATLWordChecker_i_h__
#define __ATLWordChecker_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IWordChecker_FWD_DEFINED__
#define __IWordChecker_FWD_DEFINED__
typedef interface IWordChecker IWordChecker;

#endif 	/* __IWordChecker_FWD_DEFINED__ */


#ifndef __WordChecker_FWD_DEFINED__
#define __WordChecker_FWD_DEFINED__

#ifdef __cplusplus
typedef class WordChecker WordChecker;
#else
typedef struct WordChecker WordChecker;
#endif /* __cplusplus */

#endif 	/* __WordChecker_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "shobjidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IWordChecker_INTERFACE_DEFINED__
#define __IWordChecker_INTERFACE_DEFINED__

/* interface IWordChecker */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IWordChecker;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("15e336d9-dd41-4a80-bbf7-38cb378393c0")
    IWordChecker : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE CheckWord( 
            /* [in] */ CHAR *pWordForCheck,
            /* [out] */ LONG *pResult) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IWordCheckerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IWordChecker * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IWordChecker * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IWordChecker * This);
        
        HRESULT ( STDMETHODCALLTYPE *CheckWord )( 
            IWordChecker * This,
            /* [in] */ CHAR *pWordForCheck,
            /* [out] */ LONG *pResult);
        
        END_INTERFACE
    } IWordCheckerVtbl;

    interface IWordChecker
    {
        CONST_VTBL struct IWordCheckerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IWordChecker_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IWordChecker_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IWordChecker_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IWordChecker_CheckWord(This,pWordForCheck,pResult)	\
    ( (This)->lpVtbl -> CheckWord(This,pWordForCheck,pResult) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IWordChecker_INTERFACE_DEFINED__ */



#ifndef __ATLWordCheckerLib_LIBRARY_DEFINED__
#define __ATLWordCheckerLib_LIBRARY_DEFINED__

/* library ATLWordCheckerLib */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_ATLWordCheckerLib;

EXTERN_C const CLSID CLSID_WordChecker;

#ifdef __cplusplus

class DECLSPEC_UUID("20fb3a38-5eae-4539-990b-f4bfbe167e98")
WordChecker;
#endif
#endif /* __ATLWordCheckerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


