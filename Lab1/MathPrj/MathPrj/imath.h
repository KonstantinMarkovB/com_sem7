/* MIDL: this ALWAYS GENERATED file contains the definitions for the interfaces */

//#include <atlhost.h>

// {72F0192A-9FFD-49AF-B30C-7A98BDB6988F}
DEFINE_GUID(CLSID_Math,
	0x72f0192a, 0x9ffd, 0x49af, 0xb3, 0xc, 0x7a, 0x98, 0xbd, 0xb6, 0x98, 0x8f);

// {6637ED4C-39ED-48DD-B526-F49C3FFA9942}
DEFINE_GUID(IID_IMath,
	0x6637ed4c, 0x39ed, 0x48dd, 0xb5, 0x26, 0xf4, 0x9c, 0x3f, 0xfa, 0x99, 0x42);

class IMath : public IUnknown
{
public:
	STDMETHOD(Add(long, long, long*))      PURE;
	STDMETHOD(Subtract(long, long, long*)) PURE;
	STDMETHOD(Multiply(long, long, long*)) PURE;
	STDMETHOD(Divide(long, long, long*))   PURE;

};