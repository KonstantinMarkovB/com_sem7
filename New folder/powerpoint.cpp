/* Программа получает IDispatch установленного Microsoft PowerPoint, создает новую презентацию, добавляет в неё один
* слайд и сохраняет созданную презентацию на диск по пути c:\\!work\\Presentation.pptx
*/
#include <locale.h>
#include <iostream>
#include <ObjBase.h>

using namespace std;

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");

	// инициализируем библиотеку COM
	cout << "Инициализация библиотеки COM" << endl;
	if (FAILED(CoInitialize(NULL)))
	{
		cerr << "Ошибка. Нельзя инициализировать COM." << endl;
		system("pause");
		return 1;
	}

	// получаем CLSID для PowerPoint.Application
	WCHAR szwPPProgID[] = L"PowerPoint.Application";
	CLSID clsid;
	HRESULT hr = ::CLSIDFromProgID(szwPPProgID, &clsid);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить CLSID для PowerPoint.Application. HR = " << hr << endl;
		system("pause");
		return 2;
	}

	// Получаем интерфейс IDispatch для PowerPoint.Application
	IDispatch* pIDispatch = NULL;
	hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void**)&pIDispatch);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить IDispatch для объекта Application. HR = " << hr << endl;
		system("pause");
		return 3;
	}

	DISPID dispIDPresentations;         // коллекция Presentations
	DISPID dispIDAdd;                   // метод Add коллекции Presentations
	DISPID dispIDActivePresentation;    // свойство ActivePresentation
	DISPID dispIDSlides;                // коллекция Slides объекта Presentation
	DISPID dispIDAddSlide;              // свойство Slide - результат метода Slides.Add()
	DISPID dispIDSaveAs;                // метод SaveAs объекта Presentation

	// получаем у объекта верхнего уровня Application свойство-коллекцию Presentations, чтобы добавить новую презентацию
	OLECHAR* name = L"Presentations";
	hr = pIDispatch->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDPresentations);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для коллекции Presentations. HR = " << hr << endl;
		system("pause");
		return 4;
	}

	DISPPARAMS noparams = { NULL, NULL, 0, 0 };
	VARIANT result;	// для получения результатов
	EXCEPINFO excepinfo; // для обработки ошибок
	UINT error;

	hr = pIDispatch->Invoke(dispIDPresentations, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Presentations для объекта Application. HR = " << hr << endl;
		system("pause");
		return 5;
	}
	IDispatch* pDispatchPresentations = result.pdispVal; // получили коллекцию Presentations

	// добавляем новую презентацию в коллекцию методом Add()
	// [id(0x000007d3), helpcontext(0x0007f714)]
	// Presentation* Add([in, optional, defaultvalue(-1)] MsoTriState WithWindow);
	name = OLESTR("Add");
	hr = pDispatchPresentations->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDAdd);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода Presentations.Add(). HR = " << hr << endl;
		system("pause");
		return 6;
	}
	DISPPARAMS dpAdd = { NULL, NULL, 0, 0 };
	VARIANTARG x;
	VariantInit(&x);
	x.vt = VT_ERROR;
	x.scode = DISP_E_PARAMNOTFOUND;
	dpAdd.cArgs = 1;
	dpAdd.rgvarg = &x;
	hr = pDispatchPresentations->Invoke(dispIDAdd, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dpAdd, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод Presentations.Add(). HR = " << hr << endl;
		system("pause");
		return 7;
	}

	// получаем доступ к созданной презентации через свойство ActivePresentation объекта Application
	// можно было это сделать и через коллекцию Presentations - корректнее
	name = OLESTR("ActivePresentation");
	hr = pIDispatch->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDActivePresentation);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для свойства Application.AсtivePresentation. HR = " << hr << endl;
		system("pause");
		return 8;
	}
	hr = pIDispatch->Invoke(dispIDActivePresentation, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Application.AсtivePresentation. HR = " << hr << endl;
		system("pause");
		return 9;
	}
	IDispatch* pDispatchPresentation = result.pdispVal;

	// для полученной презентации получаем коллекцию Slides
	name = OLESTR("Slides");
	hr = pDispatchPresentation->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDSlides);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для коллекции Presentation.Slides. HR = " << hr << endl;
		system("pause");
		return 10;
	}
	hr = pDispatchPresentation->Invoke(dispIDSlides, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Slides для объекта Presentation. HR = " << hr << endl;
		system("pause");
		return 11;
	}
	IDispatch* pDispatchSlides = result.pdispVal; // получили коллекцию Presentations

	// добавляем новый слайд в коллекцию Slides методом Add(). В библиотеке типов метод описан так:
	// [id(0x000007d4), hidden, helpcontext(0x00081655)]
	// Slide* Add(
	//  [in] int Index,
	//  [in] PpSlideLayout Layout);
	name = OLESTR("Add");
	hr = pDispatchSlides->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDAddSlide);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода Add коллекции Slides. HR = " << hr << endl;
		system("pause");
		return 12;
	}
	DISPPARAMS dpAddSlide = { NULL, NULL, 0, 0 };
	VARIANTARG vAddparams[2];
	dpAddSlide.cArgs = 2;
	dpAddSlide.rgvarg = vAddparams;

	// передаем параметры методу Add() в обратном порядке
	vAddparams[0].vt = VT_I4;
	vAddparams[0].lVal = 11;    // тип слайда из библиотеки типов из enum'а PpSlideLayout - ppLayoutTitleOnly = 11
	vAddparams[1].vt = VT_I4;
	vAddparams[1].lVal = 1;     // первый слайд  
	pDispatchSlides->Invoke(dispIDAddSlide, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dpAddSlide, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод Add коллекции Slides. HR = " << hr << endl;
		system("pause");
		return 13;
	}
	IDispatch* pDispatchSlide = result.pdispVal; // объект Slide коллекции Slides


//	[id(0x000007d3), propget, helpcontext(0x00081a3b)]
//	HRESULT Shapes([out, retval] Shapes * *Shapes);
	DISPID dispIDShapes;
	name = OLESTR("Shapes");
	hr = pDispatchSlide->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDShapes);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для коллекции Slides.Shapes. HR = " << hr << endl;
		system("pause");
		return 10;
	}
	hr = pDispatchSlide->Invoke(dispIDShapes, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Shapes для объекта Slide. HR = " << hr << endl;
		system("pause");
		return 11;
	}
	IDispatch* pDispatchShapes = result.pdispVal; // получили коллекцию Shapes



	// сохраняем полученную презентацию методом SaveAs для ActivePresentation
	// void SaveAs(
	//  [in] BSTR FileName,
	//  [in, optional, defaultvalue(11)] PpSaveAsFileType FileFormat,
	//  [in, optional, defaultvalue(-2)] MsoTriState EmbedTrueTypeFonts);
	name = OLESTR("SaveAs");
	hr = pDispatchPresentation->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDSaveAs);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода SaveAs объекта Presentation. HR = " << hr << endl;
		system("pause");
		return 14;
	}
	DISPPARAMS dpSaveAs = { NULL, NULL, 0, 0 };
	VARIANTARG vSaveAsparams[3];
	dpSaveAs.cArgs = 3;
	dpSaveAs.rgvarg = vSaveAsparams;

	// передаем параметры методу SaveAs() в обратном порядке
	vSaveAsparams[0].vt = VT_ERROR;     // EmbedTrueTypeFonts
	vSaveAsparams[0].scode = DISP_E_PARAMNOTFOUND;
	vSaveAsparams[1].vt = VT_I4;
	vSaveAsparams[1].lVal = 24;         // ppSaveAsFileType::ppSaveAsOpenXMLPresentation = 24
	BSTR bstrFileName = SysAllocString(OLESTR("D:\\Presentation.pptx"));
	vSaveAsparams[2].vt = VT_BSTR;      // имя файла для сохранения результата
	vSaveAsparams[2].bstrVal = bstrFileName;
	hr = pDispatchPresentation->Invoke(dispIDSaveAs, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dpSaveAs, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод SaveAs объекта Presentation. HR = " << hr << endl;
		system("pause");
		return 15;
	}
	::SysFreeString(bstrFileName);

	CoUninitialize();
	cout << "Библиотека COM отключена" << endl;
	system("pause");
	return 0;
}