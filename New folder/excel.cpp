/* Использование OLE Automation на основе SDK без использования ATL или MFC
* Программа получает IDispatch установленного Microsoft Excel

* Microsoft Excel. Вывести надпись «Hello World!» в одну из ячеек таблицы.
* Задать для надписи полужирное начертание и красный цвет.

* и сохраняет созданный файл на диск по пути D:\\excel.pptx
*/

#include <locale.h>
#include <iostream>
#include <ObjBase.h>

using namespace std;

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");

	// инициализируем библиотеку COM
	cout << "Инициализация библиотеки COM" << endl;
	if (FAILED(CoInitialize(NULL)))
	{
		cerr << "Ошибка. Нельзя инициализировать COM." << endl;
		system("pause");
		return 1;
	}

	// получаем CLSID для Excel.Application
	WCHAR szwPPProgID[] = L"Excel.Application";
	CLSID clsid;
	HRESULT hr = ::CLSIDFromProgID(szwPPProgID, &clsid);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить CLSID для Excel.Application. HR = " << hr << endl;
		system("pause");
		return 2;
	}

	// Получаем интерфейс IDispatch для Excel.Application
	IDispatch* pIDispatch = NULL;
	hr = CoCreateInstance(clsid, NULL, CLSCTX_LOCAL_SERVER, IID_IDispatch, (void**)& pIDispatch);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить IDispatch для объекта Excel.Application. HR = " << hr << endl;
		system("pause");
		return 3;
	}

	VARIANT result;       // результат
	EXCEPINFO excepinfo;  // для информативной обработки ошибок
	UINT error;           // для информативной обработки ошибок

	//=============================================== свойство Visible ================================================
	// [id(0x0000022e), propput, helpcontext(0x0002086d)]
	// HRESULT Visible( 	[in, lcid] long lcid,  	[in] VARIANT_BOOL RHS);
	OLECHAR* name = L"Visible";
	DISPID dispIDVisible;
	hr = pIDispatch->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDVisible);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить GetIdsOfName для свойства Visible. HR = " << hr << endl;
		system("pause");
		return 4;
	}

	VARIANT* arg = new VARIANT[1];
	arg[0].vt = VT_I4; arg[0].lVal = 1;
//	VARIANTARG pArgs[1];   pArgs[0].vt = VT_I4;  pArgs[0].lVal = 1;
	
	DISPPARAMS dp = { NULL, NULL, 0, 0 };
	dp.cArgs = 1;
	dp.rgvarg = arg;
	dp.cNamedArgs = 1;
	DISPID dispid = DISPID_PROPERTYPUT;
	dp.rgdispidNamedArgs = &dispid;

	hr = pIDispatch->Invoke(dispIDVisible, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dp, &result, &excepinfo, &error);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить Invoke для свойства Visible. HR = " << hr << endl;
		system("pause");
		return 5;
	}
	
	//=============================================== коллекция Workbooks ================================================
	// [id(0x0000023c), propget, helpcontext(0x0002cb4b)]
	// HRESULT Workbooks([out, retval] Workbooks * *RHS);
	DISPID dispIDWorkbooks;
	name = L"Workbooks";
	hr = pIDispatch->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDWorkbooks);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для коллекции Workbooks. HR = " << hr << endl;
		system("pause");
		return 6;
	}

	DISPPARAMS noparams = { NULL,  NULL, 0, 0 };
	hr = pIDispatch->Invoke(dispIDWorkbooks, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Workbooks для объекта Excel.Application. HR = " << hr << endl;
		system("pause");
		return 7;
	}

	IDispatch* pDispatchWorkbooks = result.pdispVal; // получили коллекцию Workbooks

	//=======================================  метод Add коллекция Workbooks =============================================
	// [id(0x000000b5), helpcontext(0x00031941)]
	// HRESULT Add(  [in, optional] VARIANT Template,  	[in, lcid] long lcid, 	[out, retval] Workbook * *RHS);
	DISPID dispIDWorkbooksAdd;
	name = L"Add";
	hr = pDispatchWorkbooks->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDWorkbooksAdd);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода Add коллекции Workbooks. HR = " << hr << endl;
		system("pause");
		return 8;
	}

	//nrequired.vt = VT_ERROR; 	nrequired.scode = DISP_E_PARAMNOTFOUND;
	hr = pDispatchWorkbooks->Invoke(dispIDWorkbooksAdd, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод Workbooks.Add(). HR = " << hr << endl;
		system("pause");
		return 9;
	}

	IDispatch* pDispatchWorkbook = result.pdispVal;

	//====================================== свойство ActiveSheet объекта Workbook ========================================
	// [id(0x00000133), propget, helpcontext(0x000309a4)]
	// HRESULT ActiveSheet([out, retval] IDispatch** RHS);
	DISPID dispIDActivesheet;
	name = L"ActiveSheet";
	hr = pDispatchWorkbook->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDActivesheet);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для свойства Application.AсtiveSheet. HR = " << hr << endl;
		system("pause");
		return 10;
	}

	hr = pDispatchWorkbook->Invoke(dispIDActivesheet, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Application.AсtiveSheet. HR = " << hr << endl;
		system("pause");
		return 11;
	}
	IDispatch* pDispatchWorksheet = result.pdispVal;

	//=============================================== свойство Name объекта Worksheet ================================================
	// [id(0x0000006e), propput, helpcontext(0x0002a800)]
	// HRESULT Name([in] BSTR RHS);
	name = L"Name";
	DISPID dispIDName;
	hr = pDispatchWorksheet->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDName);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить GetIdsOfName для свойства Worksheet.Name. HR = " << hr << endl;
		system("pause");
		return 12;
	}

	VARIANTARG pArgsName[1];
	pArgsName[0].vt = VT_BSTR;
	pArgsName[0].bstrVal = SysAllocString(L"My list");

	dp = { NULL, NULL, 0, 0 };
	dp.cArgs = 1;
	dp.rgvarg = pArgsName;
	dp.cNamedArgs = 1;
	dp.rgdispidNamedArgs = &dispid;

	hr = pDispatchWorksheet->Invoke(dispIDName, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dp, &result, &excepinfo, &error);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить Invoke для свойства Worksheet.Name. HR = " << hr << endl;
		system("pause");
		return 13;
	}

	//============================================== объект Range объекта Worksheet ==============================================
	/*[id(0x000000c5), propget, helpcontext(0x0002ac10)]
	HRESULT Range( 		[in] VARIANT Cell1,  	[in, optional] VARIANT Cell2, 		[out, retval] Range** RHS); */
	DISPID dispIDRange;
	name = L"Range";
	hr = pDispatchWorksheet->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDRange);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для объекта Worksheet.Range. HR = " << hr << endl;
		system("pause");
		return 14;
	}

	VARIANT xx; 	xx.vt = VT_BSTR; 	xx.bstrVal = SysAllocString(L"A1");
	VARIANT* pRange = new VARIANT[1]; 	pRange[0] = xx;
	dp = { NULL, NULL, 0, 0 };
	dp.cArgs = 1;
	dp.rgvarg = pRange;

	hr = pDispatchWorksheet->Invoke(dispIDRange, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &dp, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить объект Worksheet.Range. HR = " << hr << endl;
		system("pause");
		return 15;
	}
	IDispatch* pDispatchRange = result.pdispVal;


	//=============================================== свойство Value объекта Range ==============================================
	/*[id(0x00000006), propput, helpcontext(0x00023358)]
	void Value(		[in, optional] VARIANT RangeValueDataType, 		[in] VARIANT rhs); */
	name = L"Value";
	DISPID dispIDRangeValue;
	hr = pDispatchRange->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDRangeValue);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить GetIdsOfName для свойства Range.Value. HR = " << hr << endl;
		system("pause");
		return 16;
	}

	VARIANTARG pArgsValue[1];
	pArgsValue[0].vt = VT_BSTR;
	pArgsValue[0].bstrVal = SysAllocString(L"Hello World!");

	dp = { NULL, NULL, 0, 0 };
	dp.cArgs = 1;
	dp.rgvarg = pArgsValue;
	dp.cNamedArgs = 1;
	dp.rgdispidNamedArgs = &dispid;

	hr = pDispatchRange->Invoke(dispIDRangeValue, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dp, &result, &excepinfo, &error);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить Invoke для свойства Range.Value. HR = " << hr << endl;
		system("pause");
		return 17;
	}
	

	//==================================================== объект Font объекта Range ============================
	DISPID dispIDFont;
	name = L"Font";
	hr = pDispatchRange->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDFont);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для свойства Range.Font. HR = " << hr << endl;
		system("pause");
		return 18;
	}

	hr = pDispatchRange->Invoke(dispIDFont, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYGET, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить свойство Range.Font. HR = " << hr << endl;
		system("pause");
		return 19;
	}
	IDispatch* pDispatchFont = result.pdispVal;

	//======================================= свойство Bold объекта Font ============================================
	//  [propput, helpcontext(0x000887e2)]
	// HRESULT _stdcall Bold([in] VARIANT RHS);
	name = L"Bold";
	DISPID dispIDBold;
	hr = pDispatchFont->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDBold);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить GetIdsOfName для свойства Font.Bold. HR = " << hr << endl;
		system("pause");
		return 20;
	}

	VARIANTARG pArgsBold[1];
	pArgsBold[0].vt = VT_I4;
	pArgsBold[0].iVal = 1;

	dp = { NULL, NULL, 0, 0 };
	dp.cArgs = 1;
	dp.rgvarg = pArgsBold;
	dp.cNamedArgs = 1;
	dp.rgdispidNamedArgs = &dispid;

	hr = pDispatchFont->Invoke(dispIDBold, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dp, &result, &excepinfo, &error);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить Invoke для свойства Font.Bold. HR = " << hr << endl;
		system("pause");
		return 21;
	}

	//==================================================== свойство Color объекта Font =============================================
	// [propput, helpcontext(0x000887e3)]
	//	HRESULT _stdcall Color([in] VARIANT RHS);
	name = L"Color";
	DISPID dispIDColor;
	hr = pDispatchFont->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDColor);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить GetIdsOfName для свойства Font.Color. HR = " << hr << endl;
		system("pause");
		return 22;
	}

	VARIANTARG pArgsColor[1];
	pArgsColor[0].vt = VT_BSTR; 
	pArgsColor[0].bstrVal = SysAllocString(L"255");

	dp = { NULL, NULL, 0, 0 };
	dp.cArgs = 1;
	dp.rgvarg = pArgsColor;
	dp.cNamedArgs = 1;
	dp.rgdispidNamedArgs = &dispid;

	hr = pDispatchFont->Invoke(dispIDColor, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_PROPERTYPUT, &dp, &result, &excepinfo, &error);
	if (FAILED(hr)) {
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось выполнить Invoke для свойства Font.Color. HR = " << hr << endl;
		system("pause");
		return 23;
	}

	//===================================================== метод Save объекта Workbook ==================================
	/* [id(0x0000011c), hidden, helpcontext(0x000309e9)]
	HRESULT _SaveAs(
		[in, optional] VARIANT Filename,
		[in, optional] VARIANT FileFormat,
		[in, optional] VARIANT Password,
		[in, optional] VARIANT WriteResPassword,
		[in, optional] VARIANT ReadOnlyRecommended,
		[in, optional] VARIANT CreateBackup,
		[in, optional, defaultvalue(1)] XlSaveAsAccessMode AccessMode,
		[in, optional] VARIANT ConflictResolution,
		[in, optional] VARIANT AddToMru,
		[in, optional] VARIANT TextCodepage,
		[in, optional] VARIANT TextVisualLayout,
		[in, lcid] long lcid); */
	DISPID dispIDSaveAs;
	name = L"SaveAs";
	hr = pDispatchWorkbook->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDSaveAs);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода SaveAs объекта Workbook. HR = " << hr << endl;
		system("pause");
		return 24;
	}

	DISPPARAMS dpSaveAs = { NULL, NULL, 0, 0 };
	VARIANTARG vSaveAsparams[1];
	dpSaveAs.cArgs = 1;
	dpSaveAs.rgvarg = vSaveAsparams;

	BSTR bstrFileName = SysAllocString(OLESTR("D:\\Excel.xlsx"));
	vSaveAsparams[0].vt = VT_BSTR;      // имя файла для сохранения результата
	vSaveAsparams[0].bstrVal = bstrFileName;
	hr = pDispatchWorkbook->Invoke(dispIDSaveAs, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &dpSaveAs, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод SaveAs объекта Workbook. HR = " << hr << endl;
		system("pause");
		return 25;
	}
	::SysFreeString(bstrFileName);

	//============================================== метод Close объекта Workbook ==========================================
	/*  [id(0x00000115), helpcontext(0x000309ad)]
	HRESULT Close([in, optional] VARIANT SaveChanges,	[in, optional] VARIANT Filename,[in, optional] VARIANT RouteWorkbook, 	[in, lcid] long lcid); */
	DISPID dispIDClose;
	name = L"Close";
	hr = pDispatchWorkbook->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDClose);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода Close объекта Workbook. HR = " << hr << endl;
		system("pause");
		return 26;
	}

	hr = pDispatchWorkbook->Invoke(dispIDClose, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод Close объекта Workbook. HR = " << hr << endl;
		system("pause");
		return 27;
	}
	//============================================ метод Quit объекта Application ================================
	// [id(0x0000012e), helpcontext(0x0002084a)]
	// HRESULT Quit();
	DISPID dispIDQuit;
	name = L"Quit";
	hr = pIDispatch->GetIDsOfNames(IID_NULL, &name, 1, LOCALE_USER_DEFAULT, &dispIDQuit);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось получить DISPID для метода Quit объекта Workbook. HR = " << hr << endl;
		system("pause");
		return 28;
	}

	hr = pIDispatch->Invoke(dispIDQuit, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &noparams, &result, &excepinfo, &error);
	if (FAILED(hr))
	{
		cout.setf(ios::hex, ios::basefield);
		cout << "Не удалось вызвать метод Quit объекта Workbook. HR = " << hr << endl;
		system("pause");
		return 29;
	}

	CoUninitialize();
	cout << "Библиотека COM отключена" << endl;
	system("pause");
	return 0;
}